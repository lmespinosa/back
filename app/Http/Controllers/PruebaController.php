<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Datos;
use App\Prueba;
use Illuminate\Support\Facades\Validator;

class PruebaController extends Controller
{
    public function getSelect()
    {
        return Datos::all();
    }

    public function storePrueba(Request $request)
    {        
        $validatedData = Validator::make($request->all(),[
            'correo' => 'required|email',            
        ]);

        if ($validatedData->fails())
        {
            return "ERROR API: El correo no cuenta con un formato valido";
        }

        $prueba = Prueba::create($request->all());

        return response()->json($prueba, 201);        
    }

    public function getDatos()
    {
        return Prueba::all();
    }
}

