<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prueba extends Model
{
    protected $table = 'prueba';

    protected $fillable = ['correo', 'select1','select2'];

}
