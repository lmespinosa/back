<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Api extends Model
{
    public function all()
    {
        return "[
            'first_name' => 'Taylor',
            'last_name' => 'Otwell',
            'title' => 'Developer',
        ]";
    }
}
